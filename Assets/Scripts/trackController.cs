using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trackController : MonoBehaviour
{
    public float velocidad;

    public float fuerzaSalto;

    public int saltosMaximos;

    public LayerMask capaSuelo;
    public AudioClip sonidoSalto;

    private Rigidbody2D rigidbody;

    private BoxCollider2D boxCollider;

    private bool mirarDerecha = true;

    private int saltosRestantes;

    private Animator animator;

    

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        saltosRestantes = saltosMaximos;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        procesarMovimiento();
        procesamientoDeSalto();
    }

    bool estaEnElSuelo()
    {
        RaycastHit2D raycastHit =
            Physics2D
                .BoxCast(boxCollider.bounds.center,
                new Vector2(boxCollider.bounds.size.x,
                    boxCollider.bounds.size.y),
                0f,
                Vector2.down,
                0.2f,
                capaSuelo);
        return raycastHit.collider != null;
    }

    void procesamientoDeSalto()
    {

        if(estaEnElSuelo()){
            saltosRestantes = saltosMaximos;
        }


        if (Input.GetKeyDown(KeyCode.Space) && saltosRestantes > 0)
        {
            saltosRestantes --;
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0f);
            rigidbody.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
            AudioManager.Instance.ReproducirSonido(sonidoSalto); 
        }
    }

    void procesarMovimiento()
    {

        //Logica del movimiento
        float inputMovimiento = Input.GetAxis("Horizontal");

        if(inputMovimiento != 0f){
            animator.SetBool("isRunning", true);
        }
        else{
            animator.SetBool("isRunning", false);
        }

        

        rigidbody.velocity =
            new Vector2(inputMovimiento * velocidad, rigidbody.velocity.y);

        gestionOrientacion (inputMovimiento);
    }

    void gestionOrientacion(float inputMovimiento)
    {
        //Si el personaje mira a la derecha es true, pero si es a la izquierda es false
        if (
            (mirarDerecha == true && inputMovimiento < 0) ||
            (mirarDerecha == false && inputMovimiento > 0)
        )
        {
            //Ejecutamos la condicion de flip del personaje
            mirarDerecha = !mirarDerecha;
            transform.localScale =
                new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }
}
